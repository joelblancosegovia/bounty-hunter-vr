﻿using UnityEngine;

public class Orbits : MonoBehaviour
{
    //real value of gravitational constant is 6.67408 × 10-11
    //can increase to make thing go faster instead of increase timestep of Unity
    readonly float G = 1000f;
    float rotationSpeed = 0f;
    float dampAmount = 1f;
    GameObject[] celestials;

    [SerializeField]
    bool IsElipticalOrbit = false;

    // Start is called before the first frame update
    void Start()
    {
        celestials = GameObject.FindGameObjectsWithTag("Celestial");

        SetInitialVelocity();

        foreach (GameObject a in celestials)
        {
            if (a.name == "Moon")
            {
                Quaternion moonRotation = Quaternion.Euler(5, 0, 0);
                a.transform.rotation = moonRotation;
            }
        }
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        RotatePlanets();
        Gravity();
    }

    void SetInitialVelocity()
    {
        foreach (GameObject a in celestials)
        {
            foreach (GameObject b in celestials)
            {
                if(!a.Equals(b))
                {
                    float m2 = b.GetComponent<Rigidbody>().mass;
                    float r = Vector3.Distance(a.transform.position, b.transform.position);

                    a.transform.LookAt(b.transform);

                    if (IsElipticalOrbit)
                    {
                        // Eliptic orbit = G * M  ( 2 / r + 1 / a) where G is the gravitational constant, M is the mass of the central object, r is the distance between the two bodies
                        // and a is the length of the semi major axis (!!! NOT GAMEOBJECT a !!!)
                        a.GetComponent<Rigidbody>().velocity += a.transform.right * Mathf.Sqrt((G * m2) * ((2 / r) - (1 / (r * 1.5f))));
                    }
                    else
                    {
                        //Circular Orbit = ((G * M) / r)^0.5, where G = gravitational constant, M is the mass of the central object and r is the distance between the two objects
                        //We ignore the mass of the orbiting object when the orbiting object's mass is negligible, like the mass of the earth vs. mass of the sun
                        a.GetComponent<Rigidbody>().velocity += a.transform.right * Mathf.Sqrt((G * m2) / r);
                    }
                }
            }
        }
    }

    void RotatePlanets()
    {
        foreach(GameObject a in celestials)
        {
            switch (a.name)
            {
                case "Sun":
                    rotationSpeed = 19.97f;
                    dampAmount = 1f;
                    break;
                case "Mercury":
                    rotationSpeed = 0.1083f;
                    dampAmount = 1f;
                    break;
                case "Venus":
                    rotationSpeed = 0.0652f;
                    dampAmount = 1f;
                    break;
                case "Earth":
                    rotationSpeed = 15.74f;
                    dampAmount = 1f;
                    break;
                case "Moon":
                    rotationSpeed = 1.67f;
                    dampAmount = 1f;
                    break;
                case "Mars":
                    rotationSpeed = 8.66f;
                    dampAmount = 1f;
                    break;
                case "Jupiter":
                    rotationSpeed = 455.83f;
                    dampAmount = 1f;
                    break;
                case "Saturn":
                    rotationSpeed = 368.40f;
                    dampAmount = 1f;
                    break;
                case "Uranus":
                    rotationSpeed = 147.94f;
                    dampAmount = 1f;
                    break;
                case "Neptune":
                    rotationSpeed = 97.19f;
                    dampAmount = 1f;
                    break;
                default:
                    Debug.Log("Planet " + a.name + " not found.");
                    break;
            }

            a.transform.Rotate((Vector3.up * rotationSpeed) * (Time.deltaTime * dampAmount), Space.Self);

        }
    }

    void Gravity()
    {
        foreach (GameObject a in celestials)
        {
            foreach (GameObject b in celestials)
            {
                if (!a.Equals(b))
                {
                    float m1 = a.GetComponent<Rigidbody>().mass;
                    float m2 = b.GetComponent<Rigidbody>().mass;
                    float r = Vector3.Distance(a.transform.position, b.transform.position);

                    a.GetComponent<Rigidbody>().AddForce((b.transform.position - a.transform.position).normalized * (G * (m1 * m2) / (r * r)));
                }
            }
        }
    }
}
