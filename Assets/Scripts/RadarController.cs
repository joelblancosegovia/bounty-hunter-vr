using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class RadarController : MonoBehaviour
{
    [SerializeField] private List<EnemyController> m_Enemies = new();

    private void Awake()
    {
        
    }

    public void OnRadarEnter(EnemyController enemy)
    {
        m_Enemies.Add(enemy);
    }

    public void OnRadarExit(EnemyController enemy)
    {
        m_Enemies.Remove(enemy);
    }
}
