using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class initialConfig : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody>().useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Rigidbody>().useGravity)
        {
            GetComponent<Rigidbody>().useGravity = false;
        }

        

    }
}
