using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarTrigger : MonoBehaviour
{
    [SerializeField] private RadarController m_Radar;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(string.Format("Enter {0}", other));
        m_Radar.OnRadarEnter(other.gameObject.GetComponent<EnemyController>());
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log(string.Format("Exit {0}", other));
        m_Radar.OnRadarExit(other.gameObject.GetComponent<EnemyController>());
    }

}
