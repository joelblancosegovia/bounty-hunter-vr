using Autohand;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentShip : MonoBehaviour
{

    PhysicsGadgetJoystick joystick;
    GameObject ship;
    Vector2 joystickForce;
    


    // Start is called before the first frame update
    void Start()
    {
        joystick = GameObject.FindGameObjectWithTag("Joystick").GetComponent<PhysicsGadgetJoystick>();
        ship = GameObject.FindGameObjectWithTag("Ship");
    }

    // Update is called once per frame
    void Update()
    {
        joystickForce = joystick.GetValue();

        ship.transform.eulerAngles = new Vector3(joystickForce.y * 3, ship.transform.eulerAngles.y, joystickForce.x * 3);

    }
}
